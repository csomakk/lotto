﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lotto1
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int biggestNumber = 90;
        int numberOfCalls = 5;

        List<int> numbersCalled = new List<int>();

        StrategyCollection strategies = new StrategyCollection();

        public MainWindow()
        {
            InitializeComponent();
          
            resetGenerator();
        }

        private void AddStrategySelectors()
        {
            for (int i = 0; i < strategies.GetStrategies().Count; i++)
            {
                DrawStrategy currentStrategy = strategies.GetStrategies()[i];
                RadioButton newRadioButton = new RadioButton
                {
                    Content = currentStrategy.readableName,
                    GroupName = "DrawStrategySelector"
                };
                stackPanel.Children.Add(newRadioButton);
            }
        }

        public void resetGenerator()
        {
            numbersCalled = new List<int>();
            actionButton.Content = "Start";
            updateStrategySelectorEnablement(true);
        }

        public void updateStrategySelectorEnablement(bool to)
        {
            foreach (RadioButton radioButton in stackPanel.Children)
            {
                radioButton.IsEnabled = to;
            }
        }

        private DrawStrategy GetSelectedStrategySelector()
        {
            int selectedId = 0;
            for (int i = 0; i < stackPanel.Children.Count; i++) 
            {
                RadioButton currentRadioButton = (RadioButton) stackPanel.Children[i];
                if (currentRadioButton.IsChecked == true)
                {
                    selectedId = i;
                }
            }
            return strategies.GetStrategies()[selectedId];
        }

        public void initialiseDraw()
        {
            finaliseStrategy();
            updateStrategySelectorEnablement(false);
        }

        private void finaliseStrategy()
        {
            numberOfCalls = GetSelectedStrategySelector().countOfNumbers;
            biggestNumber = GetSelectedStrategySelector().biggestNumber;
        }

        public void generateNextNumber()
        {
            int newNumber = getNumber(biggestNumber, numbersCalled);
            currentNumber.Content = newNumber;
            numbersCalled.Add(newNumber);
            allNumbersSoFar.Content = String.Join(", ", numbersCalled);
        }

        public int getNumber(int biggestNumber, List<int> forbiddenNumbers )
        {
            Random randomGenerator = new Random();
            int proposedNumber;
            do
            {
                proposedNumber = randomGenerator.Next(1, biggestNumber + 1);
            }
            while (forbiddenNumbers.Contains(proposedNumber));
            return proposedNumber;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (numbersCalled.Count == 0)
            {
                initialiseDraw();
            }
            else if (numbersCalled.Count == numberOfCalls)
            {
                resetGenerator();
                return;
            }

            generateNextNumber();
            actionButton.Content = "Huzd ki az " + (numbersCalled.Count + 1) + "szamot";
            if (numbersCalled.Count == numberOfCalls)
            {
                actionButton.Content = "Reset";
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AddStrategySelectors();
        }
    }
}
