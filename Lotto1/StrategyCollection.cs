﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto1
{
    class StrategyCollection
    {
        List<DrawStrategy> strategies = new List<DrawStrategy>();

        public StrategyCollection ()
        {
            strategies.Add(new DrawStrategy(90 ,5, "5os lotto"));
            strategies.Add(new DrawStrategy(45, 6, "6os lotto"));
            strategies.Add(new DrawStrategy(35, 7, "skandinav lotto"));
            strategies.Add(new DrawStrategy(80, 10, "keno"));
        }

        public List<DrawStrategy> GetStrategies()
        {
            return strategies;
        }
    }
}
