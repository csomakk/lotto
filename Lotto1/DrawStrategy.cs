﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto1
{
    class DrawStrategy
    {
        public int biggestNumber;
        public int countOfNumbers;
        public string readableName;

        public DrawStrategy(int biggestNumber, int countOfNumbers, string readableName)
        {
            this.biggestNumber = biggestNumber;
            this.countOfNumbers = countOfNumbers;
            this.readableName = readableName;
        }
    }
}
